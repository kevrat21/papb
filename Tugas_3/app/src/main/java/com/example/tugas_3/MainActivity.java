package com.example.tugas_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    CheckBox cbkumis, cbjanggut, cbalis, cbrambut;
    ImageView imgkumis, imgjanggut, imgalis, imgrambut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cbkumis = findViewById(R.id.cbkumis);
        cbjanggut = findViewById(R.id.cbjanggut);
        cbalis = findViewById(R.id.cbalis);
        cbrambut = findViewById(R.id.cbrambut);

        imgkumis = findViewById(R.id.imgkumis);
        imgjanggut = findViewById(R.id.imgjanggut);
        imgalis = findViewById(R.id.imgalis);
        imgrambut = findViewById(R.id.imgrambut);

        cbkumis.setOnClickListener(this);
        cbjanggut.setOnClickListener(this);
        cbalis.setOnClickListener(this);
        cbrambut.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if(view.getId()==cbkumis.getId())
        {
            if(imgkumis.getVisibility()==View.INVISIBLE)
            {
                imgkumis.setVisibility(View.VISIBLE);
            }else{
                imgkumis.setVisibility(View.INVISIBLE);
            }
        }

        if(view.getId()==cbjanggut.getId())
        {
            if(imgjanggut.getVisibility()==View.INVISIBLE)
            {
                imgjanggut.setVisibility(View.VISIBLE);
            }else{
                imgjanggut.setVisibility(View.INVISIBLE);
            }
        }

        if(view.getId()==cbalis.getId())
        {
            if(imgalis.getVisibility()==View.INVISIBLE)
            {
                imgalis.setVisibility(View.VISIBLE);
            }else{
                imgalis.setVisibility(View.INVISIBLE);
            }
        }

        if(view.getId()==cbrambut.getId())
        {
            if(imgrambut.getVisibility()==View.INVISIBLE)
            {
                imgrambut.setVisibility(View.VISIBLE);
            }else{
                imgrambut.setVisibility(View.INVISIBLE);
            }
        }

    }
}