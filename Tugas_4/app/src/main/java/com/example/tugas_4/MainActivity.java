package com.example.tugas_4;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        EditText namaInput = findViewById(R.id.tf_Nama);
        EditText nimInput = findViewById(R.id.tf_nim);
        Button btnAdd = findViewById(R.id.btn_add);

        List<Items> heroList = new ArrayList<>();
        heroList.add(new Items(
                R.drawable.ic_launcher_foreground,
                "Henry",
                "215150400111003"
        ));
        heroList.add(new Items(
                R.drawable.ic_launcher_foreground,
                "Simon Petrus",
                "2151504xxxxxxxx"
        ));
        heroList.add(new Items(
                R.drawable.ic_launcher_foreground,
                "Indomie Goreng",
                "2151504xxxxxxxx"
        ));

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nama = namaInput.getText().toString();
                String nim = nimInput.getText().toString();

                Items newItems = new Items(
                        R.drawable.ic_launcher_foreground,
                        nama,
                        nim);

                heroList.add(newItems);

                // Reset text field after submit
                namaInput.getText().clear();
                nimInput.getText().clear();

                // Hide soft keyboard after submit
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(namaInput.getWindowToken(), 0);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_hero);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new ItemsAdapter(this, heroList, new ItemsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Items item) {
                // Handle item click here if needed
            }
        }));
    }
}
