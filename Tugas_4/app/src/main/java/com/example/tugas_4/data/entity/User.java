package com.example.tugas_4.data.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class User {
    @PrimaryKey(autoGenerate = true)
    private Integer uid;

    @ColumnInfo(name = "full_name")
    private String fullName;

    @ColumnInfo(name = "nim")
    private String nim;

    // Buatlah konstruktor, getter, dan setter sesuai kebutuhan.
}