package com.example.tugas_4;

import android.os.Parcel;
import android.os.Parcelable;

public class Items implements Parcelable {
    private int imageHero;
    private String nameHeroes;
    private String subnameHeroes;

    public Items(int imageHero, String nameHeroes, String subnameHeroes) {
        this.imageHero = imageHero;
        this.nameHeroes = nameHeroes;
        this.subnameHeroes = subnameHeroes;
    }

    protected Items(Parcel in) {
        imageHero = in.readInt();
        nameHeroes = in.readString();
        subnameHeroes = in.readString();
    }

    public static final Creator<Items> CREATOR = new Creator<Items>() {
        @Override
        public Items createFromParcel(Parcel in) {
            return new Items(in);
        }

        @Override
        public Items[] newArray(int size) {
            return new Items[size];
        }
    };

    public int getImageHero() {
        return imageHero;
    }

    public void setImageHero(int imageHero) {
        this.imageHero = imageHero;
    }

    public String getNameHeroes() {
        return nameHeroes;
    }

    public void setNameHeroes(String nameHeroes) {
        this.nameHeroes = nameHeroes;
    }

    public String getSubnameHeroes() {
        return subnameHeroes;
    }

    public void setSubnameHeroes(String subnameHeroes) {
        this.subnameHeroes = subnameHeroes;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(imageHero);
        parcel.writeString(nameHeroes);
        parcel.writeString(subnameHeroes);
    }
}
