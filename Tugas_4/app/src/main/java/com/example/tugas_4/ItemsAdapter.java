package com.example.tugas_4;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder> {
    private final Context context;
    private final List<Items> items;
    private final OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(Items item);
    }

    public ItemsAdapter(Context context, List<Items> items, OnItemClickListener listener) {
        this.context = context;
        this.items = items;
        this.listener = listener;
    }

    public class ItemsViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageHero;
        private final TextView nameHeroes;
        private final TextView subnameHeroes;

        public ItemsViewHolder(View view) {
            super(view);
            imageHero = view.findViewById(R.id.img_item);
            nameHeroes = view.findViewById(R.id.tv_item_name);
            subnameHeroes = view.findViewById(R.id.tv_item_subname);
        }

        public void bindingView(Items item, OnItemClickListener listener) {
            imageHero.setImageResource(item.getImageHero());
            nameHeroes.setText(item.getNameHeroes());
            subnameHeroes.setText(item.getSubnameHeroes());

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    @NonNull
    @Override
    public ItemsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_hero, parent, false);
        return new ItemsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemsViewHolder holder, int position) {
        holder.bindingView(items.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }
}
