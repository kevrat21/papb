package com.example.tugas5kotlin
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.tugas5kotlin.data.entity.User

class ItemsAdapter(var list: List<User>)
    : RecyclerView.Adapter<ItemsAdapter.ItemsViewHolder>() {

    private lateinit var dialog: Dialog

    fun setDialog(dialog: Dialog){
        this.dialog = dialog
    }
    interface Dialog{
        fun onClick(position: Int)
    }

    inner class ItemsViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val imageHero: ImageView
        val nameHeroes: TextView
        val subnameHeroes: TextView

        init {
            imageHero = view.findViewById(R.id.img_item)
            nameHeroes = view.findViewById(R.id.tv_item_name)
            subnameHeroes = view.findViewById(R.id.tv_item_subname)
            view.setOnClickListener {
                dialog.onClick(layoutPosition)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemsViewHolder {

        return ItemsViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_hero, parent,false)
        )
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ItemsViewHolder, position: Int) {
        holder.nameHeroes.text = list[position].fullName
        holder.subnameHeroes.text = list[position].nim
        holder.imageHero.setImageResource(R.drawable.profile_pic)
    }
}


