package com.example.tugas4papb

import android.annotation.SuppressLint
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DividerItemDecoration

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.VERTICAL
import com.example.tugas4papb.data.AppDatabase
import com.example.tugas4papb.data.entity.User


class MainActivity : AppCompatActivity() {
    private lateinit var database: AppDatabase
    private lateinit var adapter: ItemsAdapter
    private var list = mutableListOf<User>()
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val namaInput = findViewById<EditText>(R.id.tf_Nama)
        val nimInput = findViewById<EditText>(R.id.tf_nim)
        val btnAdd = findViewById<Button>(R.id.btn_add)

        recyclerView = findViewById<RecyclerView>(R.id.rv_hero)
        database = AppDatabase.getInstance(applicationContext)
        adapter = ItemsAdapter(list)
        adapter.setDialog(object : ItemsAdapter.Dialog{

            //Menghapus data tertentu dengan menekan view item
            override fun onClick(position: Int) {
                val dialog = AlertDialog.Builder(this@MainActivity)
                dialog.setTitle( list[position].fullName +" Hapus?")
                dialog.setItems(R.array.items_option, DialogInterface.OnClickListener{ dialog,
                     witch ->
                    if (witch==0){
                        // Hapus Data
                        Toast.makeText(applicationContext, list[position].fullName + " Berhasil dihapus", Toast.LENGTH_SHORT).show()
                        database.userDao().delete(list[position])
                        getData()
                    }else{
                        // Batal hapus
                        dialog.dismiss()
                    }
                })
                // Menampilkan dialog
                val dialogView = dialog.create()
                dialogView.show()
            }

        })

        recyclerView.layoutManager = LinearLayoutManager(applicationContext, VERTICAL, false)
        recyclerView.addItemDecoration(DividerItemDecoration(applicationContext, VERTICAL))
        recyclerView.adapter = adapter

        btnAdd.setOnClickListener {
            val nama = namaInput.text.toString()
            val nim = nimInput.text.toString()

            if (nama.isNotEmpty() && nim.isNotEmpty()){
                database.userDao().insertAll(User(
                    null,
                    nama,
                    nim
                ))
                Toast.makeText(applicationContext, "Berhasil di tambahkan", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(applicationContext, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show()
            }

            namaInput.text.clear()
            nimInput.text.clear()

            //Hide softkeyboard after submit
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(namaInput!!.windowToken, 0)

            getData()
        }

    }

    override fun onResume() {
        super.onResume()
        getData()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun getData(){
        list.clear()
        list.addAll(database.userDao().getAll())
        adapter.notifyDataSetChanged()
    }
}