package com.example.tugas2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityTwo extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_two);

        TextView viewUsername = findViewById(R.id.textViewUsername);
        TextView viewPass = findViewById(R.id.textViewPass);

        Intent intent = getIntent();
        String username = intent.getStringExtra("EXTRA_USERNAME");
        String password = intent.getStringExtra("EXTRA_PASSWORD");

        viewUsername.setText("Username: " + username);
        viewPass.setText("Password: " + password);
    }
}
