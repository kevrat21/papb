package com.example.tugas2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ActivityOne extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one);

        final Button btnSubmit = findViewById(R.id.loginButton);
        final EditText Username = findViewById(R.id.username);
        final EditText Password = findViewById(R.id.pass);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = Username.getText().toString();
                String password = Password.getText().toString();
                Intent intent = new Intent(ActivityOne.this, ActivityTwo.class);
                intent.putExtra("EXTRA_USERNAME", username);
                intent.putExtra("EXTRA_PASSWORD", password);
                startActivity(intent);
            }
        });
    }
}
